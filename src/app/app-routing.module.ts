import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/authguards/auth.guard';
import { ChatPageRoutingModule } from './pages/chat-list/chat-list-routing.module';
import { MarketPlacePageRoutingModule } from './pages/market-place/market-place-routing.module';
import { PagesModule } from './pages/pages.module';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'folder/Inbox',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'folder/:id',
  //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  // },
  {
    path: 'consumer',
    loadChildren: './pages/pages.module#PagesModule',
    data:  { role: 'Consumer'},
    canActivate: [AuthGuard]
  },
  {
    path: 'driver',
    loadChildren: './pages/pages.module#PagesModule',
    data:  { role: 'Driver' },
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    loadChildren: './pages/pages.module#PagesModule'
  },
  { 
    path: '', 
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  {
    path: 'marketplace',
    loadChildren: './pages/market-place/market-place.component.module#MarketPlaceComponentModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'chat',
    loadChildren: './pages/chat-list/chat-list.component.module#ChatListComponentModule',
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    MarketPlacePageRoutingModule,
    ChatPageRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
