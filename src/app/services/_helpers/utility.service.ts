import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { StorageService } from './storage.service';
import M from "materialize-css/dist/js/materialize.min.js";
import { MaterializeAction } from 'angular2-materialize';
import 'materialize-css';
import { AlertsService } from './alerts.service';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {

  public loader = false;
  constructor(public storage: StorageService, public alerts: AlertsService) { }

  showLoader() { 
    this.loader = true;
  }

  hideLoader() { 
    this.loader = false;
  }

  presentSuccessToast(msg) {
    // this.toaster.success(msg)
  }

  presentFailureToast(msg) {
    // this.toaster.error(msg);
  }

  returnCurrentUserUid() {
    return new Promise(async (resolve) => {
      let uid = (await this.storage.getKey('uid')) as string;
      resolve(uid);
    });
  }

  presentAlert(msg) {
    console.log(msg)
    Swal.fire({
      title: 'Alert',
      text: msg,
    });
  }

  presentSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: msg,
    });
  }

  showToast(msg, type) {

    if(type == 'success'){
      this.alerts.presentSuccessToast(msg);
    }else{
      this.alerts.presentFailureToast(msg);
    }

    //M.toast({ html: msg, classes: type == 'success' ? 'dlvr-success-toast' : 'dlvr-error-toast' })
    
  }
}
