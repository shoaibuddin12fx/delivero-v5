import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: StorageMap) { }

  getKey(key){
    return new Promise( resolve => {
      this.storage.get(key).subscribe( (data) => resolve(data) );
    })
  }

  setKey(key, value){
    return new Promise( resolve => {
      this.storage.set(key, value).subscribe( () => resolve() );
    })
  }

  removeKey(key){
    return new Promise( resolve => {
      this.storage.delete(key).subscribe( () => resolve() );
    })
  }

  clearAll(){
    return new Promise( resolve => {
      this.storage.clear().subscribe( () => resolve() );
    })
  }

  getSessionKey(key){
    return new Promise( resolve => {
      resolve(sessionStorage.getItem(key))
    })
  }

  setSessionKey(key, value){
    return new Promise( resolve => {
      resolve(sessionStorage.setItem(key, value))
    })
  }

  removeSessionKey(key){
    return new Promise( resolve => {
      resolve(sessionStorage.removeItem(key))
    })
  }

  clearAllSession(){
    return new Promise( resolve => {
      resolve(sessionStorage.clear())
    })
  }



}
