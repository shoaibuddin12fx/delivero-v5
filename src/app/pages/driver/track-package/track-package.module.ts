import { AgmCoreModule } from "@agm/core";
import { NgModule } from "@angular/core";
import { AgmDirectionModule } from "agm-direction";
import { MaterializeModule } from "angular2-materialize";

@NgModule({
    imports: [
        AgmCoreModule,
        AgmDirectionModule,
        MaterializeModule
    ],
    declarations: [
      
    ],
    providers: [
      
    ]
})
export class TrackPackageModule {
}