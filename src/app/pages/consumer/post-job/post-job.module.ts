import { NgModule } from "@angular/core";
import {CalendarModule } from "ion2-calendar";

@NgModule({
    imports: [
        CalendarModule
    ],
    declarations: [
    ],
    providers: [
      
    ]
})
export class PostJobModule {
}