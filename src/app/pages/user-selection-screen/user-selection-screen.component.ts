import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServicesService } from 'src/app/services/common-services.service';
// import 'materialize-css';
// import {MaterializeAction} from 'angular2-materialize';

@Component({
  selector: 'app-user-selection-screen',
  templateUrl: './user-selection-screen.component.html',
  styleUrls: ['./user-selection-screen.component.scss']
})
export class UserSelectionScreenComponent implements OnInit {

  // modalActions = new EventEmitter<string|MaterializeAction>();
  userRole;
  userName:any = JSON.parse(localStorage.getItem('userData'));
  loading;
  constructor(private route:Router, private commonApiService: CommonServicesService) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    if(!(localStorage.getItem('token'))) {
      this.route.navigate(['auth/login']);
    }  
  }

  goAsConsumer(){
    this.changeRole('Consumer').then(() => {
      this.route.navigateByUrl('consumer/postJob');
    })  
  }

  goAsDriver(){
    this.changeRole('Driver').then(() => {
      this.commonApiService.getUserProfileData().then((res:any) => {
        if(res.profile.is_vehicle_verified){
          this.route.navigateByUrl('driver/driverDashboard');
        } else {
          this.route.navigateByUrl('driver/myVehicle');
        }
        console.log(res);
      })
    })
  }

  goToMarket(){
    localStorage.setItem('userRole', 'marketPlace');
    this.route.navigate(['marketplace']);
  }

  async trackUser(){
    await this.commonApiService.trackUser().then(data => {}).catch(err => {})
  }

  changeRole(role){
    return new Promise((resolve, reject) => {
      this.loading = true;
      this.commonApiService.userRoleChange({role: role}).then(data => { 
        localStorage.setItem('userRole', role);
        // this.trackUser();
        this.loading = false;
        resolve(true);
      }).catch(err => {
        reject(err);
      })
    })
  }
   //open modal
  //  openModal(role) {
  //   this.userRole = role;
  //   this.modalActions.emit({action:"modal",params:['open']});
  // }
}
