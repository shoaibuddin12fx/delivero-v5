import { Component, OnInit , EventEmitter} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterializeAction } from 'angular2-materialize';
import M from "materialize-css/dist/js/materialize.min.js";
import PhoneNumber from 'awesome-phonenumber';
import { CommonServicesService } from 'src/app/services/common-services.service';
import { MessagingService } from 'src/app/shared/messaging.service';
import { FacebookLoginProvider, SocialUser, GoogleLoginProvider } from 'angularx-social-login';
import { UtilityService } from 'src/app/services/_helpers/utility.service';
import { any } from 'underscore';
import { AuthService } from 'src/app/services/authguards/auth.service';




@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  modalActions = new EventEmitter<string|MaterializeAction>();
  signupForm: FormGroup;
  displayUserDetail;
  showDetail: boolean = false;
  picturefile;
  regionCode = '+1';
  step = 'step1';
  userOTP;
  isSocialLoginInitiated = false;
  contactNumber;
  config = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '-',
    inputStyles: {
      'width': '48px',
      'height': '56px',
      "border": 'none',
      "background": '#e9f0f2',
      "border-radius": '6px',
      "margin-left": '10px'
    }
  };

  constructor(
    private authService: AuthService,
    private route: Router,
    private commonService: CommonServicesService,
    private utilityService: UtilityService,
    public activatedRoute: ActivatedRoute,
  ) {
    this.isSocialLoginInitiated = this.activatedRoute.snapshot.queryParams.isSocialLoginInitiated;
    console.log(this.isSocialLoginInitiated);
    
  }

  ngOnInit() {

    let code = localStorage.getItem('countryCode');
    this.regionCode = code ? code : '+1'
    
    this.signupForm = new FormGroup({
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      password: new FormControl(null, [Validators.required, Validators.minLength(5)]),
      email: new FormControl(null, [Validators.email, Validators.required]),
      countryCode: new FormControl('+' + PhoneNumber.getCountryCodeForRegionCode( this.regionCode ), [Validators.required]),
      contact: new FormControl(null, Validators.required),
      // age: new FormControl(null, Validators.required),
      // 'gender': new FormControl('female', Validators.required),
      street: new FormControl(null, Validators.required),
      state: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      unit: new FormControl(null, Validators.required),
      zip: new FormControl(null, Validators.required)
    });

  }

  signupUser() {
    if (this.signupForm.valid) {
      const formVal = this.signupForm.controls;
      const phNoValid = this.commonService.isPhoneNumberValid(
        this.contactNumber
      );

      console.log(phNoValid)
      if (phNoValid) {
        const data = {
          first_name: formVal.first_name.value,
          last_name: formVal.last_name.value,
          password: formVal.password.value,
          email: formVal.email.value.toLowerCase(),
          contact: this.contactNumber,
          // age : parseInt(formVal.age),
          // gender : formVal.gender,
          street : formVal.street.value,
          state : formVal.state.value,
          city : formVal.city.value,
          unit : formVal.unit.value,
          zip : formVal.zip.value
        };
        this.commonService.userSignUp(data).then((res: any) => {
          this.goForLogin();
        }).catch(err => { console.log(err); });
      } else {
        this.signupForm.controls['contact'].setErrors({ valid: false });
        this.signupForm.invalid;
      }
    }
  }

  goForLogin() {
    // this.route.navigate(['auth/login']);
    this.route.navigate(['auth/userRoleSelection']);
  }

  onVerify() {
    this.contactNumber = this.signupForm.controls['countryCode'].value.toString() + this.contactNumber;
    this.contactNumber = this.contactNumber.replace('(', '').replace(')', '').replace('-', '').replace(' ', '');
    console.log(this.contactNumber)
    localStorage.setItem('countryCode',this.signupForm.controls['countryCode'].value.toString());
    const phNoValid = this.commonService.isPhoneNumberValid(
        this.contactNumber
    );
    console.log(phNoValid);
    if (phNoValid) {
      const data = {
        contact: this.contactNumber,
        forgetpassword: false
      };
      // call getOTPforMobileNumberVerification API
      this.commonService.getOTPforMobileNumberVerification(data).then((res: any) => {
        (res.status === 200) ? this.step = 'step2' : null;
        document.getElementById('truckprogress').style.width = '20%';
        this.userOTP = res.otp;
        // let first:any = document.getElementById('firstNum');
        // let second:any = document.getElementById('secondNum');
        // let third:any= document.getElementById('thirdNum');
        // let forth:any = document.getElementById('fourthNum');
        // first.value = otp[0];
        // second.value = otp[1];
        // third.value = otp[2];
        // forth.value = otp[3];
      }).catch(err => {
        console.log(err);
        this.route.navigate(['auth/login']);
      });
    }
  }

  onOtpChange(otp) {
    this.userOTP = otp;
  }

  verifyOTP() {
    // const first: any = document.getElementById('firstNum');
    // const second: any = document.getElementById('secondNum');
    // const third: any = document.getElementById('thirdNum');
    // const fourth: any = document.getElementById('fourthNum');
    // this.userOTP = first.value + second.value + third.value + fourth.value;

    const data = { otp : parseInt(this.userOTP), forgetpassword: false };
    this.commonService.verifyOTPforMobileNumberVerification(data).then((res: any) => {
      if (res.status === 200) {
        if (this.isSocialLoginInitiated) {
          this.commonService.getUserProfileData().then(async res => {
            this.route.navigate(['auth/userRoleSelection']);
          });
        } else {
          this.step = 'step3';
          document.getElementById('truckprogress').style.width = '40%';
        }
      }
    }).catch(err => {
      console.log(err);
      // this.route.navigate(['auth/login'])
    });
  }

  // goToSignupSecond(){
  //   let data = { otp : this.userOTP };
  //   this.commonService.verifyOTPforMobileNumberVerification(data).then((res:any) => {
  //     (res.status == 200) ? this.closeModal() : null;
  //     this.step = 'step2';
  //   })
  // }

  goToSignupThird() {
      this.step = 'step3';
      document.getElementById('truckprogress').style.width = '60%';
  }

  goToSignupFourth() {
      this.step = 'step4';
      document.getElementById('truckprogress').style.width = '80%';
  }

  // Open  Modal
  openModel(modal) {
    const elems = document.getElementById(modal);
    const instances = M.Modal.getInstance(elems);
    instances.open();
  }
​
  // close modal
  closeModel(modal) {
    const elems = document.getElementById(modal);
    const instances = M.Modal.getInstance(elems);
    instances.close();
  }


  // Send New Code from 'having trouble' Popup
  sendNewCode() {
    this.onVerify();
    this.closeModel('modalTroubleList');
  }

  // Use Diff Num from 'having trouble' Popup
  useDiffNum() {
    this.step = 'step1';
    this.closeModel('modalTroubleList');
  }

  signInWithGoogle() {
    // this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
    //   const formData = this.signupForm;
    //   formData.controls['first_name'].setValue(res.firstName);
    //   formData.controls['last_name'].setValue(res.lastName);
    //   formData.controls['email'].setValue(res.email.toLowerCase());
    //   formData.controls.email.disable();
    //   this.utilityService.showToast('You have successfully registred. Please verify your mobile number and other details.', 'success');
    // });
  }
}
