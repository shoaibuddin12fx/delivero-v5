import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import PhoneNumber from 'awesome-phonenumber';
import { CommonServicesService } from 'src/app/services/common-services.service';
import { errorMessages } from 'src/app/helpers/error_messages';
import { AuthService } from 'src/app/services/authguards/auth.service';
import { MessagingService } from 'src/app/shared/messaging.service';



@Component({ 
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginSuccessful: boolean = false;
  regionCode = localStorage.getItem('countryCode');

  constructor(
    private route: Router,
    private commonService: CommonServicesService,
    private messagingService: MessagingService,
    private authService: AuthService,
  ) {
    if (!(localStorage.getItem('introVisited'))) {
      this.route.navigate(['auth/introductionScreen']);
    }

    // create form
    this.loginForm = new FormGroup({
      email: new FormControl('anupresy@gmail.com', [Validators.email, Validators.required]),
      // 'countryCode': new FormControl(PhoneNumber.getCountryCodeForRegionCode(this.regionCode), []),
      // 'mobile': new FormControl(null, []),
      password: new FormControl('@1981king', Validators.required)
    });
  }

  async isLoggedIn() {
    // Go ahead if user already signIn
    if (localStorage.getItem('currentUser')) {
      const flag = await this.authService.isAuthenticated();
      if (flag) {
        this.goToUserRoleSelection(JSON.parse(localStorage.getItem('currentUser')));
      }
    }
  }

  ngOnInit() { }

  // Go to signUp
  goToSignUp() {
    this.route.navigate(['auth/signUp']);
  }

  // After Login button clicked
  onLogin() {
    if (this.loginForm.valid) {
      const formVal = this.loginForm.value;
      // if (this.loginForm.value.mobile != null) {
      //   let phNoValid = this.commonService.isPhoneNumberValid(
          // this.loginForm.controls['countryCode'].value.toString() + this.loginForm.controls['mobile'].value);
      //   // Check mobile number entered is valid or not
      //   if (phNoValid) {
      //     delete formVal['email'];
      //     this.userLogin(formVal);
      //   } else {
      //     this.loginForm.controls['mobile'].setErrors({ valid: false });
      //     this.loginForm.invalid;
      //   }
      // } else
      if (this.loginForm.value.email != null) {
        const data = {
          email: formVal.email.toLowerCase(),
          password: formVal.password
        };
        this.userLogin(data);
      }
    }
  }

  signInWithFB() {
    // this.commonService.signInWithFB().then((res: any) => {
    //   if (res) {
    //     if (res.otp_verified) {
    //       this.goToUserRoleSelection(res);
    //     } else {
    //       this.goToMobileVarification(res);
    //     }
    //   }
    // }).catch(err => console.log(errorMessages.ERROR_SOCIAL_LOGIN, err));
  }

  async signInWithGoogle() {
    this.commonService.signInWithGoogle().then((res: any) => {
      // this.messagingService.requestPermission();
      if (res) {
        if (res.otp_verified) {
          this.goToUserRoleSelection(res);
        } else {
          this.goToMobileVarification(res);
        }
      }
    }).catch(err => {
      console.log('in error login', err);
      console.log(errorMessages.ERROR_SOCIAL_LOGIN, err);
      if (err === 'No such user') {
        this.route.navigate(['auth/signUp', { userEmail : err.email }]);
      }
    });
  }

  goToUserRoleSelection(userData) {
    this.commonService.getUserProfileData().then(async res => {
      localStorage.setItem('currentUser', JSON.stringify(userData));
      this.route.navigate(['auth/userRoleSelection']);
    });
  }

  goToMobileVarification(userData) {
    this.route.navigate(['auth/signUp'], { queryParams: { isSocialLoginInitiated: 'true' }});
  }

  userLogin(userData) {

    // this.fbService.loginViaEmailUserWithBaseLink(userData['email'], userData['password']).then( res => {
      // console.log(res);
      // this.loginSuccessful = true;
      // this.messagingService.requestPermission();
      // this.goToUserRoleSelection(res);
    // })
    this.commonService.userLogin(userData).then(async res => {
      this.loginSuccessful = true;
      // await this.messagingService.requestPermission();
      this.goToUserRoleSelection(res);
    });
  }

  goToForgotPassword() {
    this.route.navigate(['auth/forgetPassword']);
  }
}
